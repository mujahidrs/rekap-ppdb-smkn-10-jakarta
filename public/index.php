<?php
header('Access-Control-Allow-Origin:https://ppdb.jakarta.go.id');

$sekolah_id = 42010072;
$selected_jurusan = 70;
$selected_jalur = "prestasi";

if(isset($_POST['submit'])) 
{ 
	$selected_jurusan = $_POST["select_jurusan"];
	$selected_jalur = $_POST["select_jalur"];
}

$request = "https://ppdb.jakarta.go.id/seleksi/$selected_jalur/smk/1-$sekolah_id-$selected_jurusan.json";
$data = file_get_contents($request);

$list_jalur = [
	"prestasi",
	"prestasinonakademik",
	"inklusi",
	"afirmasireguler",
	"perpindahan",
];

$list_jurusan = [
	"70",
	"118",
	"119",
	"121",
];

$matriks = [];

foreach($list_jalur as $key => $jalur){
	foreach($list_jurusan as $index => $jurusan){
		$make_request = "https://ppdb.jakarta.go.id/seleksi/$jalur/smk/1-42010072-$jurusan.json";
		$matriks[$key][$index] = file_get_contents($make_request);
	}
}

foreach($list_jalur as $key => $jalur){
	foreach($list_jurusan as $index => $jurusan){
		$matriks[$key][$index] = count(json_decode($matriks[$key][$index])->data);
	}
}

function terjemah($name){
	$translate = "";

	switch($name){
		case "prestasi":
			$translate = "Prestasi";
			break;
		case "prestasinonakademik":
			$translate = "Prestasi Non Akademik";
			break;
		case "inklusi":
			$translate = "Inklusi";
			break;
		case "afirmasireguler":
			$translate = "KJP Plus dan PIP, DTKS, Mitra Transjakarta, KPJ";
			break;
		case "perpindahan":
			$translate = "Pindah Tugas Orangtua dan Anak Guru";
			break;
	}

	return $translate;
}
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Cek PPDB SMKN 10 JAKARTA</title>
		<style>
			#json_data {
				display: none;
			}

			#json_matriks {
				display: none;
			}

			.text-center {
				text-align: center;
			}
		</style>
	</head>
	<body>
		<h1>Rekap PPDB SMKN 10 JAKARTA 2021</h1>

		<div id="json_data"><?=$data?></div>

		<table border="1">
			<thead>
				<tr>
					<th rowspan="2">No.</th>
					<th rowspan="2">Jalur</th>
					<th colspan="4">Jurusan</th>
					<th rowspan="2">Jumlah</th>
				</tr>
				<tr>
					<th>RPL</th>
					<th>OTKP</th>
					<th>AKL</th>
					<th>BDP</th>
				</tr>
			</thead>
			<tbody id="table_matriks">
				<?php
					foreach($list_jalur as $key => $jalur){
						?>
						<tr>
							<td class="text-center"><?= $key+1 ?></td>
							<td><?= terjemah($jalur) ?></td>
						<?php
						foreach($list_jurusan as $index => $jurusan){
							?>
								<td class="text-center"><?= $matriks[$key][$index] ?></td>
							<?php
						}
						?>
							<td class="text-center"><?= $matriks[$key][0]+$matriks[$key][1]+$matriks[$key][2]+$matriks[$key][3] ?></td>
						</tr>
						<?php
					}
				?>
			</tbody>
			<tfoot>
				<tr>
					<th colspan="2">Total</th>
					<?php
						foreach($list_jurusan as $index => $jurusan){
							?>
								<th><?= $matriks[0][$index]+$matriks[1][$index]+$matriks[2][$index]+$matriks[3][$index]+$matriks[4][$index] ?></th>
							<?php
						}
					?>
					<th><?= ($matriks[0][0]+$matriks[1][0]+$matriks[2][0]+$matriks[3][0]+$matriks[4][0]) + ($matriks[0][1]+$matriks[1][1]+$matriks[2][1]+$matriks[3][1]+$matriks[4][1]) + ($matriks[0][2]+$matriks[1][2]+$matriks[2][2]+$matriks[3][2]+$matriks[4][2]) + ($matriks[0][3]+$matriks[1][3]+$matriks[2][3]+$matriks[3][3]+$matriks[4][3]) ?></th>
				</tr>
			</tfoot>
		</table>
		<hr>
		<h1>Data Seleksi PPDB SMKN 10 JAKARTA 2021</h1>

		<form name="test" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post">
			<span>Pilih Jalur</span>
			<select id="select_jalur" name="select_jalur">
				<option value="prestasi" <?= $selected_jalur == "prestasi" ? "selected" : "" ?>>Prestasi</option>
				<option value="prestasinonakademik" <?= $selected_jalur == "prestasinonakademik" ? "selected" : "" ?>>Prestasi Non Akademik</option>
				<option value="inklusi" <?= $selected_jalur == "inklusi" ? "selected" : "" ?>>Inklusi</option>
				<option value="afirmasireguler" <?= $selected_jalur == "afirmasireguler" ? "selected" : "" ?>>Afirmasi Reguler</option>
				<option value="perpindahan" <?= $selected_jalur == "perpindahan" ? "selected" : "" ?>>Perpindahan</option>
			</select>
			<br>
			<span>Pilih Jurusan</span>
			<select id="select_jurusan" name="select_jurusan">
				<option value="70" <?= $selected_jurusan == "70" ? "selected" : "" ?>>RPL</option>
				<option value="118" <?= $selected_jurusan == "118" ? "selected" : "" ?>>OTKP</option>
				<option value="119" <?= $selected_jurusan == "119" ? "selected" : "" ?>>AKL</option>
				<option value="121" <?= $selected_jurusan == "121" ? "selected" : "" ?>>BDP</option>
			</select>
			<br>
			<input type="submit" name="submit" value="Submit"><br>
		</form>
		<br>
		<span>Jumlah Data: </span><span id="jumlah_data"></span>
		<table border="1">
			<thead>
				<tr>
					<th>NO URUT</th>
					<th>NO DAFTAR</th>
					<th>NAMA</th>
					<th>NILAI AKHIR</th>
				</tr>
			</thead>
			<tbody id="table_data">
			</tbody>
		</table>
		<script>
			let data = JSON.parse(document.getElementById("json_data").innerText);
			console.log(data.data);

			let table_data = "";

			data.data.map(hasil => {
				table_data+=`<tr>
								<td class="text-center">${hasil[0]}</td>
								<td>${hasil[1]}</td>
								<td>${hasil[2]}</td>
								<td class="text-center">${hasil[3]}</td>
							</tr>`;
			});

			document.getElementById("table_data").innerHTML = table_data;
			document.getElementById("jumlah_data").innerText = data.data.length;
		</script>
	</body>
</html>
